fetch('http://www.omdbapi.com/?apikey=effdfdc4&s=action')
.then(response => response.json())
.then(response => {
    const action = response.Search;
    let cards = '';

    action.forEach(m => cards += showTemplate(m));
    const movieContainer = document.querySelector('.movie-action');
    movieContainer.innerHTML = cards;

    // ketika detail diklik

    const detailButton = document.querySelectorAll('.modal-detail-button');
    detailButton.forEach(btn => {
        btn.addEventListener('click', function() {
            const imdbid = this.dataset.imdbid;
            fetch('http://www.omdbapi.com/?apikey=effdfdc4&i=' + imdbid)
                .then(response => response.json())
                .then(m => {
                    const movieDetail = showMovie(m);
                    const modalBody = document.querySelector('.modal-body');
                    modalBody.innerHTML = movieDetail;
                })
        });
    });
});

fetch('http://www.omdbapi.com/?apikey=effdfdc4&s=horror')
.then(response => response.json())
.then(response => {
    const action = response.Search;
    let cards = '';

    action.forEach(m => cards += showTemplate(m));
    const movieContainer = document.querySelector('.movie-horror');
    movieContainer.innerHTML = cards;

    // ketika detail diklik

    const detailButton = document.querySelectorAll('.modal-detail-button');
    detailButton.forEach(btn => {
        btn.addEventListener('click', function() {
            const imdbid = this.dataset.imdbid;
            fetch('http://www.omdbapi.com/?apikey=effdfdc4&i=' + imdbid)
                .then(response => response.json())
                .then(m => {
                    const movieDetail = showMovie(m);
                    const modalBody = document.querySelector('.modal-body');
                    modalBody.innerHTML = movieDetail;
                })
        });
    });
});