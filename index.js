

// fetch
const searchButton = document.querySelector('.search-button');
searchButton.addEventListener('click', function() {

    const inputKeyword = document.querySelector('.input-keyword');
    fetch('http://www.omdbapi.com/?apikey=effdfdc4&s=' + inputKeyword.value)
    .then(response => response.json())
    .then(response => {
        const movie = response.Search;
        let cards = '';

        movie.forEach(m => cards += showCards(m));
        const movieContainer = document.querySelector('.movie-container');
        movieContainer.innerHTML = cards;

        // ketika detail diklik

        const detailButton = document.querySelectorAll('.modal-detail-button');
        detailButton.forEach(btn => {
            btn.addEventListener('click', function() {
                const imdbid = this.dataset.imdbid;
                fetch('http://www.omdbapi.com/?apikey=effdfdc4&i=' + imdbid)
                .then(response => response.json())
                    .then(m => {
                        const movieDetail = showMovie(m);
                        const modalBody = document.querySelector('.modal-body');
                        modalBody.innerHTML = movieDetail;
                    })
                });
            });
        });
    });
    
    class Content {
        constructor(tile) {
          this.tile = tile;
        }
        get title() {
          return this.tile;
        }
        set title(x) {
          this.tile = x;
        }
      }
      
      let myContent = new Content(smallContent());
      
      document.querySelector(".content").innerHTML = myContent.title;
    
    function smallContent(){
        return `<small>Welcome to our</small>
        <h1>WEB <br> International Movie </h1>
        <small>Please Type Something to Search for Movie!!</small>`;
    }
    
    
    // class film {
        //     constructor (movie){
//         this.movie = movie;
//         this.fungsi = function(){
//             const searching = response.Search;
//             let cards = '';
//             searching.forEach( m => cards += showCards(m));
//         }

//     }
// }
// let myMovie = new film();


// const searchButton = document.querySelector('.search-button');
// searchButton.addEventListener('click', async function() {
//     const inputKeyword = document.querySelector('.input-keyword');
//     const movies = await getMovies(inputKeyword.value);
//     updateUI(movies);
// });

// // detail
// document.addEventListener('click', async function (e) {
//     if(e.target.classList.contains('modal-detail-button')){
//         const imdbid = e.target.dataset.imdbid;
//         const movieDetail = await getMovieDetail(imdbid);
//         updateUIDetail(movieDetail);
//     }
// })

// function getMovieDetail(imdbid) {
//     fetch('http://www.omdbapi.com/?apikey=effdfdc4&i=' + imdbid)
//         .then(response => response.json())
//         .then(m => m);
// }

// function updateUIDetail(m) {
//     const movieDetail = showMovie(m);
//     const modalBody = document.querySelector('.modal-body');
//     modalBody.innerHTML = movieDetail;
// }

// function getMovies(keyword) {
//     return fetch('http://www.omdbapi.com/?apikey=effdfdc4&s=' + keyword)
//     .then(response => response.json())
//     .then(response => response.Search);

// }

// function updateUI(movies) {
//     let cards = '';
//     movies.forEach(m => cards += showCards(m));
//     const movieContainer = document.querySelector('.movie-container');
//     movieContainer.innerHTML = cards;
// }


function showCards(m) {
    return `<div class="col-md-3 my-3">
                <div class="card">
                    <img src="${m.Poster}" class="card-img-top">
                    <div class="card-body">
                    <h5 class="card-title">${m.Title}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${m.Year}</h6>
                    <a href="#" class="btn btn-primary modal-detail-button" data-bs-toggle="modal" data-bs-target="#moviedetailmodal" data-imdbid="${m.imdbID}">Show Details</a>
                    </div>
                </div>
            </div>`;
}

function showTemplate(m) {
    return `<div class="col-md-2 my-3">
                <div class="card">
                    <img src="${m.Poster}" class="card-img-top">
                    <div class="card-body">
                    <h5 class="card-title">${m.Title}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${m.Year}</h6>
                    <a href="#" class="btn btn-primary modal-detail-button" data-bs-toggle="modal" data-bs-target="#moviedetailmodal" data-imdbid="${m.imdbID}">Show Details</a>
                    </div>
                </div>
            </div>`;
}

function showMovie(m){
    return `<div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <img src="${m.Poster}" class="img-fluid">
                    </div>
                    <div class="col-md">
                        <ul class="list-group">
                            <li class="list-group-item"><h4>${m.Title} (${m.Year})</h4></li>
                            <li class="list-group-item"><strong>Director : </strong>${m.Director}</li>
                            <li class="list-group-item"><strong>Actors : </strong>${m.Actors}</li>
                            <li class="list-group-item"><strong>Writer : </strong>${m.Writer}</li>
                            <li class="list-group-item"><Strong>Plot : </Strong> <br> ${m.Plot}</li>
                        </ul>
                    </div>
                </div>
            </div>`;
}


function toggleMenu(){
    let navigation = document.querySelector('.navigation');
    let toggle = document.querySelector('.toggle');
    let header = document.querySelector('.header');
    navigation.classList.toggle('active');
    toggle.classList.toggle('active');
    header.classList.toggle('active');
}